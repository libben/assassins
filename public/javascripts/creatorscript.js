$(document).ready(function() {
  $('input[type=\'checkbox\'').on('click', function() {
    if (!$('input:checked').val()) {
      $('#submit').attr('disabled', true);
    } else {
      $('#submit').attr('disabled', false);
    }
  })
})
